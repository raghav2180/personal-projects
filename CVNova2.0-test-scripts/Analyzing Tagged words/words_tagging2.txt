{
"VBG": ["challenging", "enhancing", "implementing", "understanding", "using", "using", "using", "working", "completing", "solving", "finding", "programming", "programming", "warning", "aiming", "controlling", "depending", "travelling", "providing", "avoiding", "providing", "writing", "writing", "writing", "learning", "growing"], 

"RB": ["optimally", "ltd.", "altera", "award", "forum", "polyamide", "transmitter-receiver", "long", "effectively", "always"], 

"NN": ["rahul", "shahi", "email", "rahulshahi.1995", "contact", "b.e.", "communication", "thapar", "university", "patiala", "become", "company", "environment", "versatile", "personality", "company", "reputation", "growth", "education", "class", "class", "coding", "university", "patiala", "jyoti", "school", "azamgarh", "jyoti", "school", "azamgarh", "till", "semester", "isc", "board", "icse", "board", "programming", "structure", "application", "thinking", "ability", "figure", "design", "simulation", "development", "microprocessor", "verilog", "hardware", "description", "language", "position", "level", "chem-e-car", "competition", "car", "emission", "position", "catapult", "competition", "university", "saturnalia-2016", "student", "activity", "secretary", "society", "phoenix", "techfest", "rc", "obstacle", "track", "maze", "solver", "techfest", "robot", "maze", "way", "path", "status", "freelance", "platform", "upwork", "technical/software", "proficiency", "c", "c++", "java", "knowledge", "knowledge", "board", "infantry", "friend", "foe", "iff", "system", "prevent", "fire", "differentiating", "force", "the", "project", "fire", "condition", "rf", "technology", "home", "september", "", "december", "home", "system", "create", "secure", "living", "environment", "system", "voice", "command", "dtmf", "technology", "iii", "quadcopter", "april", "", "august", "", "system", "frame", "bldc", "frequency", "iv", "ac", "regulation", "system", "", "the", "system", "temperature", "railway", "metro", "number", "system", "use", "energy", "temperature", "solver", "robot", "", "techfest", "algorithm", "evaluate", "maze", "path", "curicullar", "i.", "secretary", "iete", "year", "chief", "mentor", "year", "gen.", "sec", "year", "society", "thapar", "university", "society", "president", "rotaract", "club", "thapar", "university", "club", "welfare", "year", "iii", "student", "co-ordinator", "week", "festival", "elementos", "feb", "feb", "team", "year", "head", "round", "championship", "university", "competition", "association", "iit", "madras", "ark", "month", "site", "world", "content", "ghost", "copy", "assistance", "project", "assistance", "information", "i.", "ability", "work", "ii", "school", "level", "basketball", "tournament", "position", "head", "house", "captain", "school", "level", "information", "hereby", "declare", "knowledge", "belief", "rahul", "shahi", "date", "july"], 

"FW": ["ece", "etc"], 

"POS": ["'s"], 

"RBR": ["quadcopter"], 

"VBD": ["enhanced", "learned", "worked", "modelled", "compiled", "secured", "secured", "held", "awarded", "", "controlled", "participated", "", "received", "renounced", "controlled", "integrated", "allowed", "managed", "mentored", "organized", "held", "rated", "worked", "represented", "held"], 

"JJS": ["techfest", "largest", "techfest", "best"], 

"VBN": ["associated", "synthesized", "designed", "participated", "held", "built", "held", "rated", "used", "built", "committed", "organized", "given"], 

"CD": ["21", "1.", "147001", "2018", "2013", "2011", "8.18/10", "7th", "80", "88.4", "6", "2017", "2.", "6", "2016", "1st", "zero", "1st", "2017", "2016", "2016", "2016", "2016", "30a", "4", "1000kv", "6", "2.4ghz", "2015", "2016", "2014", "2015", "1st", "2nd", "3rd", "250", "2016", "2017", "30+", "1st", "2nd", "3rd", "2016", "2015.", "50+", "29th", "2017"], 

"VBP": ["gmail.com", "delhi", "pvt", "vlsi", "", "build", "arduino", "atmega", "i.", "detect", "rc", "v.", "find", "", "ii", "workshops", "october"], 

"JJ": ["male", "yrs", "+91-8725833204", "current", "objective", "utilize", "", "thapar", "niketan", "niketan", "june-july", "c++", "analytical", "logical", "dkop", "june-july", "practical", "industrial", "fpga", "8-bit", "xilinx", "zonal", "iit-b", "hazardous", "thapar", "general", "technical", "iete", "", "iit-b", "stable", "capable", "iit-k", "smart", "capable", "shortest", "possible", "top", "", "basic", "various", "january", "", "present", "friendly", "allied", "hostile", "friendly", "signal", "ii", "smart", "reliable", "domestic", "iot", "esc", "automatic", "november", "january", "regulate", "", "efficient", "comfortable", "maze", "august,2015", "build", "iit-k", "smart", "robot", "shortest", "unwanted", "extra", "curricular", "extra", "general", "forum", "2017-present", "various", "vice-", "2017-present", "various", "social", "overall", "technical", "various", "technical", "iv", "zonal", "national", "thapar", "amalgam-15", "top", "freelancing", "upwork", "2014-2017", "various", "technical", "additional", "team", "zonal", "enthusiastic", "new", "iv", "2012-2013", "2011-2012", "additional", "true", "complete"], 

"IN": ["though", "though", "upon", "november", "throughout", "osc", "organizer", "around", "like", "boy"], 

"VBZ": ["provides", "trainings", "skills", "uses", "works", "makes", "undertakes", "iii"], 

"DT": ["bluetooth"], 

":": ["--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"], 

"NNS": ["electronics", "traits", "blocks", "weeks", "learnings", "skills", "data", "solutions", "labs", "weeks", "learnings", "applications", "tools", "kits", "achievements", "students", "participants", "participants", "data", "structures", "micro-controllers", "sensors", "ics", "projects", "forces", "photodiodes", "coveys", "connections", "motors", "channels", "coaches", "people", "variations", "turns", "students", "electronics", "projects", "students", "projects", "events", "events", "events", "students", "robotics", "technologies", "v.", "projects", "clients", "services", "things", "skills", "particulars", "herein"], 

"JJR": ["laser"], 

"NNP": ["xii", "x", "xilinx", "", "", "", "smart", "the", "the", "member", "the", "", "", "", "", ""]}
