from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from cStringIO import StringIO
from nltk.corpus import stopwords
from nltk import word_tokenize

class BagOfWords:
    def __init__(self):
        self.doc = doc
        self.rsrcmgr = PDFResourceManager()
        self.retstr = StringIO()
        self.laparams = LAParams()
        self.codec = 'utf-8'
        self.device = TextConverter(self.rsrcmgr,self.retstr,laparams=self.laparams,codec=self.codec)
        self.interpreter = PDFPageInterpreter(self.rsrcmgr,self.device)
        return None

    def generate_bag_words(self,doc):
        for page in PDFPage.get_pages(self.doc):
            self.interpreter.process_page(page)
        self.text = self.retstr.getvalue()
        self.text_tokens = word_tokenize(self.text.decode('utf-8'))
        self.stop_words = set(stopwords.words('english'))
        self.bag_of_words = [x for x in self.text_tokens if not x in self.stop_words]
        return self.bag_of_words


if __name__=="__main__":
    file_name = "Resume_july2017.pdf"
    doc = open(file_name,'rb')
    x = BagOfWords()
    data = x.generate_bag_words(doc)
    print(data)
    
