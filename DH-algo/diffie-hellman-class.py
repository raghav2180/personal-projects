"""

Diffie-Hellman Key Exchange Algorithm
Consider two parties A and B which has to exchange their private keys and verify their connection.

"""

public_prime_mod = 17  #Public Prime Modulo 
public_gen = 3  #Public Generator
A_secret = 15  #Secret key of A
B_secret = 13  #Secret key of B

class DH_method:
    def __init__(self,public_prime_mod,public_gen,A_secret,B_secret):
        self.public_prime_mod = public_prime_mod
        self.public_gen = public_gen
        self.A_secret = A_secret
        self.B_secret = B_secret
    
    def results_to_send(self):
        """Calculates the Results of both A and B using mod formula which are to send to each other and returns them"""
        self.A_result = (self.public_gen**self.A_secret)%(self.public_prime_mod)
        self.B_result = (self.public_gen**self.B_secret)%(self.public_prime_mod)
        return self.A_result,self.B_result

    def find_shared_keys(self):
        """Calculates the Shared Key of both A and B and return them"""
        self.A_shared = (self.B_result**self.A_secret)%(self.public_prime_mod)
        self.B_shared = (self.A_result**self.B_secret)%(self.public_prime_mod)
        return self.A_shared,self.B_shared

if __name__ == "__main__":
    x = DH_method(public_prime_mod,public_gen,A_secret,B_secret)

    A_result,B_result = x.results_to_send()
    print("A sends over public channel:",A_result)
    print("B sends over public channel:",B_result)

    A,B = x.find_shared_keys()
    print("Shared Secret calculated by A:",A)
    print("Shared Secret calculated by B:",B)

    if(A==B):
        print("Connection is successfully verified.")
    else:
        print("Connection is not verified.")


