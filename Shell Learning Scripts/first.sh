#!/bin/sh  
#This above line tells Unix that the file is to be executed by /bin/sh. This is the standard location of the Bourne shell on just about every Unix system.  #! is a special directory which Unix treats specilly. Hence it is not a comment.
# This is a comment!
echo Hello World
echo Hello     World        # This is a comment, too!
echo "Hello      world"
echo Hello * World
echo "Hello * World"
echo "Hello" World
echo Hello "   " World
echo "Hello" "*" "World"
echo 'Hello' World
