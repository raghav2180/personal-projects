from rest_framework import serializers
from urlshortner.models import Urls

class urlSerializer(serializers.ModelSerializer):
	class Meta:
		model = Urls
		fields = ('id','longurl','shorturl_id')