from django import forms
from urlshortner.models import Urls 

class URLform(forms.ModelForm):
	longurl = forms.CharField(label='Enter Your Long URL', max_length = 200)

	class Meta:
		model = Urls
		fields = ('longurl',)