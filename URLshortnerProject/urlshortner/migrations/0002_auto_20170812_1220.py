# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-12 06:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('urlshortner', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='urls',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
