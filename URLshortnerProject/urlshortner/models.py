# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Urls(models.Model):
	longurl = models.TextField()
	shorturl_id = models.SlugField(max_length=10)
	id = models.AutoField(primary_key=True)

	def __str__(self):
		return self.longurl