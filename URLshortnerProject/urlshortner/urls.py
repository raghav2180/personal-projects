from django.conf.urls import url
from . import views

app_name = 'urlshortner'
urlpatterns = [
    url(r'^$',views.home, name='home'),
    url(r'^(?P<shorturl_id>[\w\-]+)/$', views.redirect, name = 'redirect'),
    #url(r'^generate/$', views.generate, name='generate'),
    url(r'^api/getlongurls/$',views.getlongurls),
    url(r'^api/generateshorturl',views.generateshorturl)
]
