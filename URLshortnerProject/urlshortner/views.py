# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from forms import URLform
from models import Urls 
import string
from math import floor
from django.shortcuts import redirect

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from URLshortnerProject.serializers import urlSerializer

def home(request):
	if request.method == 'POST':
		form = URLform(request.POST)
		if form.is_valid():
			Lurl = form.cleaned_data['longurl']
			f = Urls.objects.filter(longurl__iexact=Lurl).values_list('shorturl_id',flat=True)
			if f:
				u = list(f)
				print("OKAY HERE")
				shorturl = u[0]
				Surl = 'http://127.0.0.1:8000/urlshortner/'+shorturl
				return render(request,'urlshortner/home.html',{'form':form, 'Lurl':Lurl,'Surl':Surl}) 
			else:
			    print("UNIQUE URL ENTERED")	
			    latest = Urls.objects.latest('id')
			    Lurl_id = latest.id + 100
			    shorturl = toBase62(Lurl_id,62)
			    Surl = 'http://127.0.0.1:8000/urlshortner/'+shorturl
			    save_to_db(Lurl, shorturl)
			    form = URLform()
			    print("Before Internal Return")
		        return render(request,'urlshortner/home.html',{'form':form, 'Lurl':Lurl,'Surl':Surl })
	else:
	    print("IN OUTER ELSE")		
	    form = URLform
	print("BEFORE OUTER RETURN")    
	return render(request, 'urlshortner/home.html', {'form':form})

def redirect(request,shorturl_id):
	i = toBase10(shorturl_id,62)
	u = Urls.objects.get(id=(i-99))
	print(u)
	Lurl = u.longurl
	#return redirect(u.longurl)
	return render(request,'urlshortner/redirect.html',{'Lurl':Lurl})

def save_to_db(long, short):
	u = Urls(longurl=long, shorturl_id=short)
	u.save()


def toBase62(num, b=62):
    if b <= 0 or b > 62:
        return 0
    base = string.digits + string.lowercase + string.uppercase
    r = num % b
    res = base[r];
    q = floor(num / b)
    while q:
        r = q % b
        q = floor(q / b)
        res = base[int(r)] + res
    return res

def toBase10(num, b = 62):
    base = string.digits + string.lowercase + string.uppercase
    limit = len(num)
    res = 0
    for i in xrange(limit):
        res = b * res + base.find(num[i])
    return res

@api_view(["GET"])
def getlongurls(request):
	if request.method == 'GET':
		urls = Urls.objects.all()
		searializer = urlSerializer(urls, many=True)
		return Response(searializer.data)

@api_view(["POST"])
def generateshorturl(request):
	if request.method == 'GET':
		urls = Urls.objects.all()
		searializer = urlSerializer(urls, many=True)
		return Response(searializer.data)
	elif request.method == 'POST':
		data = request.data
		if data.get('longurl') is None:
			return Response(status=status.HTTP_400_BAD_REQUEST)
		try:
			url, created = Urls.objects.get_or_create(
				longurl=data.get('longurl',None),
			)
		except Exception as e:
			logger.exception(e)
		if created:
			latest = Urls.objects.latest('id')
			Lurl_id = latest.id + 100
			shorturl = toBase62(Lurl_id,62)
			url.shorturl_id = shorturl
			url.save()
		return Response(request.data)		